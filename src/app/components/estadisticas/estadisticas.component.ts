import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit
} from "@angular/core";
import { DatabaseService } from "src/app/services/database.service";
import { Sectores } from "src/app/models/sectores";

@Component({
  selector: "app-estadisticas",
  templateUrl: "./estadisticas.component.html",
  styleUrls: ["./estadisticas.component.css"]
})
export class EstadisticasComponent implements AfterViewInit {
  private listPoins: any = [];
  private listSectores: any = [];
  private listDepartament: any = [];
  private listOrg: any = [];
  private sectorModel: Sectores = new Sectores();
  baseUri: any = "../../../assets/img/icons/";
  init : any = true;
  data : any;

  constructor(private db: DatabaseService) {}

  @ViewChild("mapContainer", { static: false }) gmap: ElementRef;
  map: google.maps.Map;
  lat = 4.6311106;
  lng = -73.805329;
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 5
  };

  ngAfterViewInit() {
    this.mapInitializer();
    this.db.getAllPoins().then<any>(points => {
      points.forEach(point => {
        let data = {
          id: point.payload.doc.id,
          data: point.payload.doc.data()
        };
        this.listPoins.push(data);
        this.setMarkertMAp(point.payload.doc.data());
      });

      
      console.log(this.listPoins);
      this.getSectores();
      this.getPoinsOfDepartament();
      this.getPoinsOfOrg();
    });

    window.setTimeout(() => {
     
    }, 5000);
  }

  getSectores() {
    let sectores = [];
    let listSectores = {};
    this.sectorModel.sector.forEach(sector => {
      this.listPoins.forEach(point => {
        let sect = point.data.sectores.filter(
          point => point.sector == sector.name
        );
        if (sect.length > 0) {
          sectores.push(sect[0].sector);
        }
      });
    });
    sectores.forEach(sector => {
     
      listSectores[sector] = (listSectores[sector] || 0) + 1;
      
    });


    for (let index = 0; index < Object.keys(listSectores).length; index++) {
      let element = {
        sector: Object.keys(listSectores)[index],
        cantidad : Object.values(listSectores)[index]
      };
      
      this.listSectores.push(element);    
      
      
    }

    
    console.log(this.listSectores);
    // console.log(Object.values(this.listSectores));
    // console.log(Array.from(this.listSectores));
  }

  captured(){
    window.print();
  }

  setData(filter){
    this.data = filter;
    this.init = false;
  }

  getPoinsOfDepartament() {
    let listDepartament = {};
    this.listPoins.forEach(point => {

      listDepartament[point.data.ubication.departamento] =
        (listDepartament[point.data.ubication.departamento] || 0) + 1;
    });


    for (let index = 0; index < Object.keys(listDepartament).length; index++) {
      let element = {
        sector: Object.keys(listDepartament)[index],
        cantidad : Object.values(listDepartament)[index]
      };
      
      this.listDepartament.push(element);    
      
      
    }

    console.log(this.listDepartament);
  }

  setMarkertMAp(point) {
    console.log(point);  
    let image = {
      url: this.baseUri + "Ubicación Todos.png",
      size: new google.maps.Size(90, 100),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(50, 50)
    };

    let coor = new google.maps.LatLng(point.ubication.lat, point.ubication.lnt);

    let marker = new google.maps.Marker({
      map: this.map,
      draggable: false,
      icon: image,
      position: coor
    });

    
    marker.setMap(this.map);
  }


  getPoinsOfOrg() {
    let listOrg = {};

    this.listPoins.forEach(point => {

      listOrg[point.data.orgName] =
        (listOrg[point.data.orgName] || 0) + 1;
    });

    for (let index = 0; index < Object.keys(listOrg).length; index++) {
      let element = {
        sector: Object.keys(listOrg)[index],
        cantidad : Object.values(listOrg)[index]
      };
      
      this.listOrg.push(element);    
      
      
    }

    console.log(this.listOrg);
  }

  mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
  }
}
