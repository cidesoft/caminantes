import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fEntradaTitle'
})
export class FEntradaTitlePipe implements PipeTransform {

  transform(entradas: Post[], filter: string): any {

    if (!filter || filter === '') {
      return entradas;
    }
    const result = entradas.filter(post => (
      post.title.toLowerCase().includes(filter.toLowerCase())
    ));
    return(result);


  }

}
