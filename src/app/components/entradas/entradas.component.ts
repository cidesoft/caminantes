import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-entradas',
  templateUrl: './entradas.component.html',
  styleUrls: ['./entradas.component.css']
})
export class EntradasComponent implements OnInit {

  constructor(private db: DatabaseService, private router: Router) { }

  titulo = new FormControl('')
  fecha = new FormControl('')
  fTitulo = '';
  fFecha = null;

  posts: Post[]
  ngOnInit() {
    this.init();
    
  }

  init(){
    this.db.getAllPostValues().then((posts: Post[]) => {
      this.posts = posts;
      console.log(posts);
      
    });

    this.titulo.valueChanges.pipe(
      debounceTime(800),
    ).subscribe((value) => this.fTitulo = value)

    this.fecha.valueChanges.subscribe(( x: string )  => this.fFecha = x == '0' || null || ''
    ? null : new Date( x.replace(/-/g, '/')).getTime());

  }

  delete(id){
    this.db.deletePost(id).then(ret => {
      this.init();

    })
  }


}
