import { Pipe, PipeTransform } from '@angular/core';
import { isSameDay } from 'date-fns';
@Pipe({
  name: 'fEntradasDate'
})
export class FEntradasDatePipe implements PipeTransform {

  transform(entradas: Post[], dayFilter: any): any {

    if (!dayFilter) {
      return entradas;
    }

    const result = entradas.filter(post => (
      !post.dateCreation ? false :
      isSameDay( (post.dateCreation.seconds ) * 1000, dayFilter)
    ));
    return(result);
  }

}
