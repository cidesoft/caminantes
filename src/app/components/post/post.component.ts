import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { NotifiComponent } from '../notifi/notifi.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  date: any;
  @Input() post: any;

  monst : any = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Marzp",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
  ];
   
    constructor() {  console.log(this.post);}

  ngOnInit() {
    const date = new Date(Date.now());
    console.log(this.post.text);
    this.date = date.getDate() + " de " +this.monst[date.getMonth()] + ' de ' + date.getFullYear();
    
  }

  setText(string) {
    $("#descp").empty();
    let desp = $.parseHTML(string);
    $("#descp").append(desp);
  }

}
