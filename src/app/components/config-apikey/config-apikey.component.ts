import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-config-apikey',
  templateUrl: './config-apikey.component.html',
  styleUrls: ['./config-apikey.component.css']
})
export class ConfigApikeyComponent implements OnInit {

  key : any;
  constructor(private db: DatabaseService) { }

  ngOnInit() {
    this.db.getKey().then<any>(key => {
      console.log(key)
      this.key = key[0].googleKey;
      

    });
      

  }

  setGoogleKey(){
    if(this.key != undefined){
      this.db.setConfig(this.key).then(rest => console.log(rest));
    }
    
  }
}
