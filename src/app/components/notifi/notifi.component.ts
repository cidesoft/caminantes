import { Component, OnInit } from "@angular/core";
import { DatabaseService } from "src/app/services/database.service";

@Component({
  selector: "app-notifi",
  templateUrl: "./notifi.component.html",
  styleUrls: ["./notifi.component.css"]
})
export class NotifiComponent implements OnInit {
  options = { autoHide: false, scrollbarMinSize: 10 };
  listPost: any = [];
  listResPost: any = [];
  listFromPost: any;
  post: any;
  constructor(private db: DatabaseService) {
    this.db.getAllPostValues().then(postList => {
      this.listPost = postList;
      console.log(this.listPost);
    });

  }

  setPost(post) {
    this.post = post;
  }

  moreInfo(){
    console.log('info')
  }
  
 

  ngOnInit() {
    this.listPost.forEach(post => {
      if(this.listFromPost.length < 2){
        this.listFromPost.push(post);

      }
    })
  }
}
