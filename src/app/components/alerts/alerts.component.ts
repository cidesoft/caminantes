import { Component, OnInit } from "@angular/core";
import { DatabaseService } from "src/app/services/database.service";
import { CookieService } from "ngx-cookie-service";
import { PostService } from "src/app/services/post.service";

@Component({
  selector: "app-alerts",
  templateUrl: "./alerts.component.html",
  styleUrls: ["./alerts.component.css"]
})
export class AlertsComponent implements OnInit {
  listAlerts: any = [];
  user: any;
  role: string;
  overlay: any = false;
  depa: any;
  dat: Date;
  listData: any = [];

  constructor(
    public service: DatabaseService,
    private cookieService: CookieService,
    public services: PostService
  ) {
    this.user = JSON.parse(this.cookieService.get("cookie-user"));
    this.role = this.cookieService.get("user-role");
  }

  ngOnInit() {
    this.getAlertstofRole();
    this.services.getDepartaments().subscribe(dat => {
      this.listData = dat;
    });
  }

  reloader() {
    this.getAlertstofRole();
  }

  getAlertstofRole() {
    switch (this.role) {
      case "ADMIN":
        this.overlay = true;
        this.service.getAlerts().then<any>(alert => {
          console.log(alert);
          // this.listPoins = points;
          alert.forEach(point => {
            let data = {
              id: point.payload.doc.id,
              data: point.payload.doc.data()
            };
            this.listAlerts.push(data);
          });
          console.log(this.listAlerts);
        });
        this.overlay = false;
        break;

      case "ORGANY":
        this.service
          .getAlertsOfOrgany(this.user.data.organy)
          .then<any>(points => {
            // this.listPoins = points;
            points.forEach(point => {
              let data = {
                id: point.payload.doc.id,
                data: point.payload.doc.data()
              };
              this.listAlerts.push(data);
            });
          });
        this.overlay = false;

        break;
      case "USER":
        this.service.getAlertsOfUser(this.user.id).then<any>(points => {
          // this.listPoins = points;
          points.forEach(point => {
            let data = {
              id: point.payload.doc.id,
              data: point.payload.doc.data()
            };
            this.listAlerts.push(data);
          });
        });
        this.overlay = false;

        break;
    }
  }

  buscar() {
    console.log(this.listAlerts);
    this.overlay = true;
    let data = [];

    this.listAlerts.forEach(point => {
      if (
        point.data.dateCreation >= this.dat.valueOf() ||
        point.data.departamento == this.depa
      ) {
        data.push(point);
      }
    });

    this.listAlerts = data;

    this.overlay = false;
  }

  date(time) {
    let data = new Date(time);
    let month = data.getMonth() + 1;
    return data.getDate() + " / " + month + " / " + data.getFullYear();
  }
}
