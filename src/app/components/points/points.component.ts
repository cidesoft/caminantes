import { Component, OnInit } from "@angular/core";
import { DatabaseService } from "src/app/services/database.service";
import { CookieService } from "ngx-cookie-service";
import { PostService } from "src/app/services/post.service";
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { MySheetComponent } from '../custom/my-sheet/my-sheet.component';

@Component({
  selector: "app-points",
  templateUrl: "./points.component.html",
  styleUrls: ["./points.component.css"]
})
export class PointsComponent implements OnInit {
  overlay: any = false;
  public listData: any = [];
  private listPoins: any = [];
  user: any;
  date: Date;
  depa: any;
  role: string;
  constructor(
    private db: DatabaseService,
    public service: PostService,
    private cookieService: CookieService,
    private botonSheet : MatBottomSheet
  ) {
    this.user = JSON.parse(this.cookieService.get("cookie-user"));
    this.role = this.cookieService.get("user-role");
  }

  changeStatus(data, point){
    point.data.active = true
    point.data.status = data
    let Id; 

    if(point.data.idUpdate != undefined){
      Id = point.data.idUpdate;
      console.log(point.data.title);
      this.db.deletePointUpdate(point.id);
      point.data.idUpdate = false;
      point.data.title = point.data.title.replace( ' (Actualización) ', '');
     
    }else{
      Id = point.id;
    }

    this.db.activedPoint(Id, point.data).then(rest => {
      alert('Punto Activado');
      this.listPoins = [];
      this.getPoinstofRole();

    })

  }

  openBoton() : void {
    this.botonSheet.open(MySheetComponent)
  }

  ngOnInit() {
    console.log(typeof this.role)

    this.getPoinstofRole();
    this.service.getDepartaments().subscribe(dat => {
      this.listData = dat;
    });
  }

  getPointsUpdates(){
    this.db.getPoinsUpdate().then<any>(points => {
      // this.listPoins = points;
      points.forEach(point => {
        let data = {
          id: point.payload.doc.id,
          data: point.payload.doc.data()
        };
        this.listPoins.push(data);
      });
    });
  }

  getPointsUpdatesOfOrgany(id){
    this.db.getPoinsUpdateOfOrgany(id).then<any>(points => {
      // this.listPoins = points;
      points.forEach(point => {
        let data = {
          id: point.payload.doc.id,
          data: point.payload.doc.data()
        };
        this.listPoins.push(data);
      });
    });
  }

  getPoinstofRole() {
    switch (this.role) {
      case "ADMIN":
        this.overlay = true;
        console.log(this.role)

        this.db.getAllPoinsAdmin().then<any>(points => {
          // this.listPoins = points;
            console.log(points)

          points.forEach(point => {
            let data = {
              id: point.payload.doc.id,
              data: point.payload.doc.data()
            };
            this.listPoins.push(data);
          });
          console.log(this.listPoins);
        });
        this.getPointsUpdates();
        this.overlay = false;
        break;

      case "ORGANY":
        console.log(this.user.id);
        this.db.getPoinsOfOrgany(this.user.id).then<any>(points => {
          // this.listPoins = points;
          points.forEach(point => {
            let data = {
              id: point.payload.doc.id,
              data: point.payload.doc.data()
            };
            this.listPoins.push(data);
          });
        });
        this.getPointsUpdatesOfOrgany(this.user.id);
        this.overlay = false;

        break;
      case "USER":
        this.db.getPoinsOfPropietario(this.user.id).then<any>(points => {
          // this.listPoins = points;
          points.forEach(point => {
            let data = {
              id: point.payload.doc.id,
              data: point.payload.doc.data()
            };
            this.listPoins.push(data);
          });
          console.log(this.listPoins);
        });
        this.overlay = false;

        break;
    }
  }

  dateCreation(time) {

    const monst = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Marzp",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ];
    let date = new Date(time);
    let label = monst[date.getMonth()] + ' ' + String(date.getDate()) + ' de ' + date.getFullYear();
    return label;
  }

  reloader(){
    this.getPoinstofRole();
  }

  buscar(){
    console.log(this.listPoins);
    this.overlay = true;
    let data = [];

    if(this.date){
      this.listPoins.forEach(point =>{
        if(point.data.ubication.fechaInit >= this.date.valueOf() || point.data.ubication.departamento == this.depa){
          data.push(point);
        }
      })
    }else{
      this.listPoins.forEach(point =>{
        if(point.data.ubication.departamento == this.depa){
          data.push(point);
        }
      })
    }

    

    this.listPoins = data;

    this.overlay = false;

  }

  delete(point) {

    let a  = confirm('Seguro que desea borrar el punto ' + point.data.title)
    if(a){
      this.overlay = true;
      this.db
        .deletePoint(point.id)
        .then(() => {
          this.overlay = false;
          this.listPoins = [];
          alert('Punto borrado correctamente');
          this.ngOnInit();
        })
        .catch(err => {
          console.log(err);
          this.overlay = false;
        });
    }
   
  }
}


