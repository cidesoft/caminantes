import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Output,
  EventEmitter
} from "@angular/core";
import { Points } from "src/app/models/points";
import { FormBuilder, Validators } from "@angular/forms";
import { PostService } from "src/app/services/post.service";
import { DatabaseService } from "src/app/services/database.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-point-register",
  templateUrl: "./point-register.component.html",
  styleUrls: ["./point-register.component.css"]
})
export class PointRegisterComponent implements AfterViewInit {
  @ViewChild("mapContainer", { static: false }) gmap: ElementRef;
  @ViewChild("address", { static: false }) address: ElementRef;
  @Output() change: EventEmitter<File> = new EventEmitter<File>();
  map: google.maps.Map;
  auto: google.maps.places.Autocomplete;
  lat = 4.6311106;
  lng = -73.805329;
  time: any;
  src: any;
  file: any;
  capacidad: any;
  overlay: any = false;
  user: any;
  public listOrganys: any = [];
  public listOrganysSelects: any = [];

  coordinates = new google.maps.LatLng(this.lat, this.lng);

  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 8
  };

  marker: any;
  listOrgs: any = [];
  points: Points = new Points();
  pont: Points;
  id: any;
  public listData: any = [];
  public listDays: any = [];
  public listCitys;
  public ubicationForm;
  URLPublica: any;
  role: string;
  constructor(
    public formBuilder: FormBuilder,
    public service: PostService,
    private db: DatabaseService,
    private router: Router,
    private rutaActiva: ActivatedRoute,
    private cookieService: CookieService
  ) {
    this.role = this.cookieService.get("user-role");
    this.user = JSON.parse(this.cookieService.get("cookie-user"));
    this.id = this.rutaActiva.snapshot.params.id;
    if (this.id != undefined) {
      this.setPointUpdate();
    }

    this.db.getOrganysActives().then<any>(data => {
      data.forEach(point => {
        let dat = {
          id: point.payload.doc.id,
          data: point.payload.doc.data()
        };
        this.listOrganys.push(dat);
      });
      console.log(this.listOrganys);
    });
  }

  setOrgany(org) {
    this.listOrganysSelects.push(org);
    let index = this.listOrganys.indexOf(
      this.listOrganys.find(x => x.id == org.id)
    );

    this.listOrganys.splice(index, 1);

    console.log(this.listOrganysSelects);
    console.log(this.listOrganys);

  }

  del(org){
    console.log(org);
    this.listOrganys.push(org);
    let index = this.listOrganysSelects.indexOf(
      this.listOrganysSelects.find(x => x.id == org.id)
    );

    this.listOrganysSelects.splice(index, 1);
  }

  isValit() {

    console.log(this.points.sectores);
    let sector = true;
    let grup = true;
    let hora = true;


    this.points.sectores.sector.forEach(sec =>{
      if(sec.sector){
        sector = false;

      }
    })

    this.points.grupo.grupos.forEach(grp =>{
      if(grp.grup){
        grup = false;

      }
    })

    this.points.horarys.dias.forEach(dia =>{
      if(dia.horaInic != ''){
        hora = false;

      }
    })
    
    if (this.points.title == undefined) {
      alert("Debe Colocar un titulo");
      return true;
    }else if(this.points.desc == undefined){
      alert("Debe Colocar una descripción del punto");
      return true;
    }else if(this.points.capacidad == undefined){
      alert("Debe colocar la capacidad de atención del punto");
      return true;
    }else if(this.points.oferta == undefined){
      alert("Debe colocar la oferta");
      return true;
    }else if(this.points.ubication.address == undefined){
      alert("Debe colocar una dirección");
      return true;
    }else if(this.points.ubication.municipio == undefined){
      alert("Debe colocar una ciudad");
      return true;
    }else if(this.points.ubication.fechaInit == undefined){
      alert("Debe colocar una Fecha");
      return true;
    }else if(this.points.ubication.fechaFin == undefined){
      alert("Debe colocar una Fecha");
      return true;
    }else if(this.points.contact.name == undefined){
      alert("Debe colocar un nombre de contacto");
      return true;
    }else if(this.points.contact.email == undefined){
      alert("Debe colocar un email de contacto");

      return true;
    }else if(this.points.contact.telefono == undefined){
      alert("Debe colocar telefono Fecha");
      return true;
    }else if(grup){
      alert("Debe seleccionar al menos un grupo");
      return true;
    }else if(sector){
      alert("Debe seleccionar al menos un sector");
      return true;
    }else if(hora){
      alert("Debe Configurar al menos un horario");
      return true;
    }

    





  }

  ngAfterViewInit() {
    document.getElementsByTagName("html")[0].style.overflow = "hidden";

    this.service.getDepartaments().subscribe(dat => {
      this.listData = dat;
    });
  }

  getDateLabel(date) {
    let d = new Date(date);
    let month = d.getMonth() + 1;
    return d.getDate() + "/" + month + "/" + d.getFullYear();
  }

  setPointUpdate() {
    console.log(this.router);

    this.db.getPoint(this.id).then(point => {
      this.points.title = point.title;
      this.points.desc = point.desc;
      this.points.ubication.departamento = point.ubication.departamento;
      this.onChange(point.ubication.departamento);
      this.points.ubication.municipio = point.ubication.municipio;
      this.points.ubication.address = point.ubication.address;
      this.src = point.img;
      this.points.img = point.img;
      this.points.oferta = point.oferta;
      this.points.capacidad = point.capacidad;
      this.points.ubication.fechaInit = new Date(point.ubication.fechaInit);
      this.points.ubication.fechaFin = new Date(point.ubication.fechaFin);
      this.points.contact.name = point.contact.name;
      this.points.contact.email = point.contact.email;
      this.points.contact.telefono = point.contact.telefono;
      this.file = false;
      this.points.ubication.iterante = point.ubication.iterante;
      this.points.ubication.lat = point.ubication.lat;
      this.points.ubication.lnt = point.ubication.lnt;

      let coorMarkert = new google.maps.LatLng(
        parseFloat(point.ubication.lat),
        parseFloat(point.ubication.lnt)
      );

      this.initMap(point.ubication.iterante, coorMarkert); // this.setMarkertMAp(coorMarkert);
      console.log(point);
      console.log(this.points);

      this.db.getOrganysActives().then<any>(data => {
        data.forEach(point => {
          let dat = {
            id: point.payload.doc.id,
            data: point.payload.doc.data()
          };
          this.listOrganys.push(dat);
        });
        console.log(this.listOrganys);
      });
    });
  }

  mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    let directionsDisplay = new google.maps.DirectionsRenderer({
      draggable: true,
      map: this.map,
      suppressMarkers: true
    });

    // this.map.addListener('click', data => {
    //   console.log(data.latLng.lat());
    //   this.setMarkertMAp(data.latLng);
    //   this.points.ubication.lat = data.latLng.lat();
    //   this.points.ubication.lnt = data.latLng.lng();
    // })

    this.auto = new google.maps.places.Autocomplete(this.address.nativeElement);
    this.auto.addListener("place_changed", event => {
      this.points.ubication.address = this.address.nativeElement.value;
      this.setAddress();
    });
  }

  setMarkertMAp(coor) {
    if (this.marker != undefined) {
      this.marker.setMap(null);
    }

    this.marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      position: coor,
      title: "Su posición"
    });

    this.marker.addListener("dragend", event => {
      // this.points.ubication.latLnt = event.latLng;
      this.points.ubication.lat = event.latLng.lat();
      this.points.ubication.lnt = event.latLng.lng();
    });

    this.map.setCenter(coor);
    this.marker.setMap(this.map);
  }

  async setAddress() {
    window.clearTimeout(this.time);
    this.time = window.setTimeout(async () => {
      (
        await this.service.getCoordinatesAddress(this.points.ubication.address)
      ).subscribe(coor => {
        if (coor.results.length == 0) {
          return;
        }
        let a = coor.results[0].geometry.location;
        this.points.ubication.lat = a.lat;
        this.points.ubication.lnt = a.lng;
        let coorMarkert = new google.maps.LatLng(
          parseFloat(a.lat),
          parseFloat(a.lng)
        );

        this.setMarkertMAp(coorMarkert);
      });
    }, 3000);
  }

  cambioArchivo(event) {
    this.file = event.target.files[0];
    this.projectImage(this.file);
  }

  projectImage(file: File) {
    let reader = new FileReader();
    reader.onload = (e: any) => {
      this.src = e.target.result;
      this.change.emit(file);
    };
    reader.readAsDataURL(file);
  }

  async registerPoint(status) {
    if (this.points.ubication.iterante == "0") {
      if (this.points.ubication.address == undefined) {
        alert("Debe ingresar una dirección.");
        return;
      }
    }

    if (status) {
      this.points.action = "Nuevo Borrador";
      this.points.active = false;
      this.points.status = "Borrador";
    } else {
      if (this.role == "USER") {
        this.points.action = "En revisión";
        this.points.active = false;
        this.points.status = "En Espera";
      } else {
        this.points.action = "Nuevo punto de ayuda";
        this.points.active = true;
        this.points.status = "Activo";
      }
    }

    this.setData(status);
  }

  updatePoint() {
    this.points.propietario = this.user.id;
    this.points.orgName = this.user.data.orgName;
    this.points.organizacion = this.user.data.organy;
    this.points.action = "Actualización de punto";
    this.points.active = true;
    this.points.borrador = false;
    this.points.status = "Activo";
    this.points.listOrg = this.listOrganysSelects;

    if (this.isValit()) {
      return;
    }

    if (this.file) {
      this.db.setImg(this.file).then(url => {
        this.points.img = String(url);
      });
    }

    this.updatePointDb();
  }

  updatePointDb() {
    this.overlay = true;
    console.log(this.points);
    if(this.role == 'USER'){
      this.points.title += ' (Actualización) ';
      this.points.status = "En espera";
      this.db.updatePointUser(this.id, this.points).then(() => {
        alert('Punto modificado, en espera de autorización.');
        this.overlay = false;
        this.router.navigateByUrl("admin");
      }).catch(error => {
        console.log(error);
      });

    }else{

      this.db.updatePoint(this.id, this.points).then(() => {
        this.overlay = false;
        alert('Punto modificado Satisfactoriamente');

        this.router.navigateByUrl("admin");
      });
    }

   

    this.overlay = false;
  }

  setData(status) {
    if (this.isValit()) {
      return;
    }
    this.overlay = true;

    if (this.file) {
      this.db.setImg(this.file).then(url => {
        this.points.img = String(url);
        this.points.propietario = this.user.id;
        this.points.borrador = status;
        this.points.listOrg =  this.listOrganysSelects;
        this.points.orgName = this.user.data.orgName;
        if (this.role == "ADMIN" || this.role == "ORGANY") {
          this.points.organizacion = this.user.id;
        } else {
          this.points.organizacion = this.user.data.organy;
        }

        this.db.setNewPoint(this.points).then(() => {
          this.overlay = false;
          alert("Punto creado satisfactoriamente");
          this.router.navigateByUrl("/admin");
        });
      });
    } else {
      this.overlay = false;

      alert("Debe colocar una imagen");
      return;
    }

    // this.points.ubication = this.ubication;
    // this.poi

    this.overlay = false;
  }

  initMap(val, coor: any = false) {
    console.log(val);
    if (val == 0) {
      window.setTimeout(() => {
        this.mapInitializer();
        if (coor) {
          this.setMarkertMAp(coor);
        }
      }, 1000);
    } else {
      this.points.ubication.address = "";
      this.points.ubication.lat = 0;
      this.points.ubication.lnt = 0;
    }
    return true;
  }

  onChange(value) {
    this.listCitys = [];
    this.listData.forEach(dat => {
      if (dat.departamento == value) {
        this.listCitys = dat.ciudades;
      }
    });
  }
}
