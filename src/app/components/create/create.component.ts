import { Component, OnInit } from "@angular/core";
import { Entradas } from "src/app/models/entradas";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { DatabaseService } from "src/app/services/database.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';



@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.css"]
})
export class CreateComponent implements OnInit {

  titlePage = 'Nueva Entrada';
  entradaForm: FormGroup;
  createModel: Entradas = new Entradas();
  file: File;
  overlay: boolean = false;
  user: any;
  config = { toolbar: ['bold', 'italic', { list: 'bullet' }, 'blockquote', { align: [] }, { font: [] }] };

  qEStyles = {
    height: '200px',
    backgroundColor: 'white'
  }
  catgList: any = [];


  constructor(
    public formBuilder: FormBuilder,
    private db: DatabaseService,
    private router: Router,
    private cookieService: CookieService,
    private route: ActivatedRoute
  ) {
    this.entradaForm = formBuilder.group({
      title: ['', [Validators.required]],
      category: ['', [Validators.required]],
      descp: ['', [Validators.required]],
      text: ['', Validators.required],
      image: ''
    });

    this.user = JSON.parse(this.cookieService.get("cookie-user"));
  }


  ngOnInit() {
    if (this.route.snapshot.params.id) {
      const id = this.route.snapshot.params.id;
      this.db.getPostById(id).then(post => this.entradaForm
        .patchValue({
          title: post.title,
          category : post.category,
          descp: post.descrip,
          text: post.text,
         // image: {name: 'dgdf'}
        }) )
      this.titlePage = 'Editar Entrada';
    }
    document.getElementsByTagName("html")[0].style.overflow = "hidden";

    this.db.getAllCategoriesConfigValues().then(catg => {
      this.catgList = catg;
      console.log(catg);
    })



  }

  cambioArchivo(event) {
    this.file = event.target.files[0];
  }

  public() {

    if (this.entradaForm.valid) {
      const {image, ...form} = this.entradaForm.value;
      alert(image);
      
      this.createModel = form;
      console.log(this.createModel);

      this.overlay = true;
      if (this.file) {
        this.createModel.propietrio = this.user.id;
        if (this.user.data.organy == "NULL") {
          this.createModel.organy = this.user.id;

        } else {
          this.createModel.organy = this.user.data.organy;
        }
        this.db.setImg(this.file).then(url => {
          this.createModel.img = String(url);
          this.db.setNewPost(this.createModel /* Pasar el */).then(() => {
            location.reload();
            this.overlay = false;
            this.router.navigate(["/admin"]);
          });
        });
      } else {
        this.overlay = false;
        alert("Debe colocar una imagen");
        return;
      }
    }
  }

}
