import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-config-organizacion',
  templateUrl: './config-organizacion.component.html',
  styleUrls: ['./config-organizacion.component.css']
})
export class ConfigOrganizacionComponent implements OnInit {

  listOrg : any;
  key: any;
  overlay: any = false;
  constructor(private db: DatabaseService) { }

  ngOnInit() {
    this.initList();
   
  }

  initList(){
    this.overlay = true;
    this.listOrg = [];

    this.db.getOrgs().then<any>(org =>{
      org.forEach(point => {
        let data = {
          id: point.payload.doc.id,
          data: point.payload.doc.data()
        };
        this.listOrg.push(data);
        this.overlay = false;
      });
      console.log(this.listOrg);
    }).catch(() => this.overlay = false);

    this.overlay = false;

  }

  setOrg(){
    if (this.key != undefined){
      this.db.setConfigOrg(this.key).then(rest => {
        this.key = '';
        this.initList();
      });
    
    }
  }

  delete(id){
    this.db.deleteOrg(id).then(() => {
      this.initList();
    })
    console.log(id)
  }



}
