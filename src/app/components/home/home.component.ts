import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { Observable } from "rxjs";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { map, shareReplay } from "rxjs/operators";
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import { DatabaseService } from "src/app/services/database.service";
import { Sectores } from "src/app/models/sectores";
import { PostService } from "src/app/services/post.service";
import { MDCRipple } from "@material/ripple";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements AfterViewInit {
  @ViewChild("mapContainer", { static: false }) gmap: ElementRef;
  @ViewChild("drawer", { static: false }) snav: any;

  map: google.maps.Map;
  lat = 4.6311106;
  lng = -73.805329;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  isHandsetInfo$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  coordinates = new google.maps.LatLng(this.lat, this.lng);
  listPoints: any = [];
  listPointsMarkerts: any = [];
  infoPoint: any = false;
  listSectores: Sectores = new Sectores();
  point: any;
  today: any;
  options = { autoHide: false, scrollbarMinSize: 100 };
  baseUri: any = "../../../assets/img/icons/";
  city: any;
  tipe: any;
  cityLabel: any = "UBICACIÓN";
  tipeLabel: any = "TIPO DE AYUDA";
  isMap: boolean = true;
  buscar: boolean = true;
  isBlog: boolean = false;
  isBlogInic: boolean = true;

  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 7
  };

  marker = new google.maps.Marker({
    position: this.coordinates,
    map: this.map,
    title: "Hello World!"
  });
  listData: Object;
  post: any;
  po: any;
  constructor(
    private breakpointObserver: BreakpointObserver,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private db: DatabaseService,
    private service: PostService
  ) {
    this.initMarkert();
    this.matIconRegistry.addSvgIcon(
      "list",
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        "../../../assets/img/icons/blog.svg"
      )
    );
    const dias = [
      "",
      "Lunes",
      "Martes",
      "Miercoles",
      "Jueves",
      "Viernes",
      "Sabado",
      "Domingo"
    ];

    this.service.getDepartaments().subscribe(dat => {
      this.listData = dat;
    });

    let date = new Date();
    this.today = dias[date.getUTCDay()];
  }

  initMarkert() {
    this.db.getAllPoinsValues().then(points => {
      console.log(points);
      this.listPoints = points;
      points.forEach(point => {
        this.setMarkertMAp(point);
      });
    });
  }

  mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.map.addListener("click", () => {
      // this.infoPoint = false;
    });
    // this.marker.setMap(this.map);
  }

  setPost(post) {
    if (post.post) {
      this.post = post.post;
      this.isBlog = false;
      jQuery("#map").hide();
    }
  }

  setMarkertMAp(point) {
    console.log(point);
    let url: any;
    if (point.ubication.lat == 0) {
      return;
    }
    if (point.multisector) {
      url = this.baseUri + "Ubicación Todos.png";
    } else {
      switch (point.sectores[0].sector) {
        case "Albergue":
          url = this.baseUri + "Alojamiento.png";
          break;

        case "WASH":
          url = this.baseUri + "Agua.png";
          break;

        case "Salud":
          url = this.baseUri + "Salud.png";
          break;

        case "Protección":
          url = this.baseUri + "Proteccion.png";
          break;

        case "Transporte Humanitario":
          url = this.baseUri + "Transporte.png";
          break;

        default:
          url = this.baseUri + "Ubicación Todos.png";
          break;
      }
    }
    let image = {
      url: url,
      size: new google.maps.Size(100, 100),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(40, 60)
    };

    let coor = new google.maps.LatLng(point.ubication.lat, point.ubication.lnt);

    let marker = new google.maps.Marker({
      map: this.map,
      draggable: false,
      icon: image,
      position: coor
    });

    this.listPointsMarkerts.push(marker);

    marker.addListener("click", () => {
      if(!this.snav._opened){
        this.snav.toggle();
      }
      this.point = point;
      this.infoPoint = true;
    });
    marker.setMap(this.map);
  }

  moreInfo() {
    $("#nav-content").addClass("expant");
    $("#float").hide();
  }

  showBlog() {
    this.post = false;
    
    if (this.isBlog) {
      this.isBlog = false;
      this.isBlogInic =  false;

      jQuery("#map").show();
      // this.isMap = true;
    } else {
      this.isBlog = true;
      this.isBlogInic =  true;

      jQuery("#map").hide();

      // this.isMap = false;
    }
  }

  toggle() {
    console.log(this.snav);
    this.snav.toggle();
    this.infoPoint = false;
    
  }

  setCity(city) {
    this.cityLabel = city.target.textContent;
    this.city = city.target.textContent;
  }

  setTipe(tipe) {
    this.tipeLabel = tipe.target.textContent;
    this.tipe = tipe.target.textContent;
  }

  // scrollhidde() {
  //   $(".mat-drawer-inner-container")[0].classList.add("scroll-hidden");
  // }

  ngAfterViewInit() {
    console.log();
    this.mapInitializer();
  }

  resetear() {
    this.buscar = true;
    this.cityLabel = "UBICACIÓN";
    this.tipeLabel = "TIPO DE AYUDA";

    this.listPointsMarkerts.forEach(mark => {
      mark.setMap(null);
    });

    this.initMarkert();
  }

  getDate(time) {
    const monst = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Marzp",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ];
    let date = new Date(time);
    return (
      "Disponible hasta: " +
      monst[date.getMonth()] +
      " " +
      date.getDate() +
      " de " +
      date.getFullYear()
    );
  }

  getPointsCustoms() {
    // location.reload();
    this.buscar = false;

    this.listPointsMarkerts.forEach(mark => {
      mark.setMap(null);
    });

    this.listPoints.forEach(point => {
      point.sectores.forEach(sector => {
        if (this.tipe != undefined && this.city != undefined) {
          if (
            sector.sector == this.tipe &&
            point.ubication.departamento == this.city
          ) {
            this.setMarkertMAp(point);
          }
        } else {
          if (
            sector.sector == this.tipe ||
            point.ubication.departamento == this.city
          ) {
            this.setMarkertMAp(point);
          }
        }
      });
    });
  }
}
