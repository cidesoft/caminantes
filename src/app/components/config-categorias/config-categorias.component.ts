import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-config-categorias',
  templateUrl: './config-categorias.component.html',
  styleUrls: ['./config-categorias.component.css']
})
export class ConfigCategoriasComponent implements OnInit {

  listCatg : any;
  key: any;
  overlay: any = false;
  constructor(private db: DatabaseService) { }

  ngOnInit() {
    this.initList();
   
  }

  initList(){
    this.overlay = true;
    this.listCatg = [];

    this.db.getCatgs().then<any>(org =>{
      org.forEach(point => {
        let data = {
          id: point.payload.doc.id,
          data: point.payload.doc.data()
        };
        this.listCatg.push(data);
        this.overlay = false;
      });
      console.log(this.listCatg);
    }).catch(() => this.overlay = false);

    this.overlay = false;

  }

  setOrg(){
    if (this.key != undefined){
      this.db.setConfigCatg(this.key).then(rest => {
        this.key = '';
        this.initList();
      });
    
    }
  }

  delete(id){
    this.db.deleteOrg(id).then(() => {
      
      this.initList();
    })
    console.log(id)
  }
}
