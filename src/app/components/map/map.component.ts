import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  map: google.maps.Map<any>;
  lat = 4.6311106;
  lng = -73.805329;
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 7
  };
  listPoints: any = [];
  listPointsMarkerts: any = [];
  baseUri: any = "../../../assets/img/icons/";
  point: any;

  constructor( private db: DatabaseService,) { }
  @ViewChild("mapContainer", { static: false }) gmap: ElementRef;

  ngAfterViewInit() {
    this.mapInitializer();
  }

  mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.map.addListener("click", () => {
      // this.infoPoint = false;
    });
    this. initMarkert();

    // this.marker.setMap(this.map);
  }

  initMarkert() {
    this.db.getAllPoinsValues().then(points => {
      console.log(points);
      this.listPoints = points;
      points.forEach(point => {
        this.setMarkertMAp(point);
      });
    });
  }

  setMarkertMAp(point) {
    console.log(point);
    let url: any;
    if (point.ubication.lat == 0) {
      return;
    }
    if (point.multisector) {
      url = this.baseUri + "Ubicación Todos.png";
    } else {
      switch (point.sectores[0].sector) {
        case "Albergue":
          url = this.baseUri + "Alojamiento.png";
          break;

        case "WASH":
          url = this.baseUri + "Agua.png";
          break;

        case "Salud":
          url = this.baseUri + "Salud.png";
          break;

        case "Protección":
          url = this.baseUri + "Proteccion.png";
          break;

        case "Transporte Humanitario":
          url = this.baseUri + "Transporte.png";
          break;

        default:
          url = this.baseUri + "Ubicación Todos.png";
          break;
      }
    }
    let image = {
      url: url,
      size: new google.maps.Size(100, 100),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(40, 60)
    };

    let coor = new google.maps.LatLng(point.ubication.lat, point.ubication.lnt);

    let marker = new google.maps.Marker({
      map: this.map,
      draggable: false,
      icon: image,
      position: coor
    });

    this.listPointsMarkerts.push(marker);

    marker.addListener("click", () => {
      // this.snav.toggle();
      this.point = point;
      // this.infoPoint = true;
    });
    marker.setMap(this.map);
  }

}
