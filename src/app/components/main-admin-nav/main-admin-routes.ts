import { Routes } from '@angular/router';
import { EstadisticasComponent } from '../estadisticas/estadisticas.component';
import { PointsComponent } from '../points/points.component';
import { PointRegisterComponent } from '../point-register/point-register.component';
import { AlertsComponent } from '../alerts/alerts.component';
import { EntradasComponent } from '../entradas/entradas.component';
import { CreateComponent } from '../create/create.component';
import { AdminComponent } from '../admin/admin.component';
import { AdminProfileComponent } from '../admin-profile/admin-profile.component';
import { ConfigComponent } from '../config/config.component';

export const AdminRoutes: Routes = [
  { path: 'estadisticas', component: EstadisticasComponent },
  { path: 'puntos', component: PointsComponent},
  { path: 'registrar-puntos', component: PointRegisterComponent},
  { path: 'registrar/:id', component: PointRegisterComponent},
  { path: 'alerts', component: AlertsComponent},
  { path: 'entradas', component: EntradasComponent},
  { path: 'entrada/:id', component: CreateComponent},
  { path: 'config', component: ConfigComponent},
  { path: 'crear-entradas', component: CreateComponent},
  { path: 'admin', component: AdminProfileComponent}
]

