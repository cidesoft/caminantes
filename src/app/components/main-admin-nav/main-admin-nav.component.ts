import { Component } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

@Component({
  selector: "app-main-admin-nav",
  templateUrl: "./main-admin-nav.component.html",
  styleUrls: ["./main-admin-nav.component.scss"]
})
export class MainAdminNavComponent {
  name: any = "ESTADISTICAS";
  content: number = 1;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  user: any;
  role: string;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private cookieService: CookieService,
    private router: Router
  ) {
    this.user = JSON.parse(this.cookieService.get("cookie-user"));
    this.role = this.cookieService.get("user-role");
    console.log(this.role);
  }

  setTitle(name) {
    this.name = name;
  }

  logaut() {
    this.cookieService.delete("cookie-user");
    this.router.navigateByUrl("");
  }
}
