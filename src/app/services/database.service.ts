import { Injectable } from "@angular/core";
// import * as firebase from "firebase";
import { AngularFirestore } from "@angular/fire/firestore";

import { User } from "../models/user";
import { Points } from "../models/points";
import { AngularFireStorage } from "@angular/fire/storage";
import { finalize, map } from "rxjs/operators";
import { resolve } from "url";
import { Entradas } from "../models/entradas";
import { registerLocaleData } from "@angular/common";

@Injectable({
  providedIn: "root"
})
export class DatabaseService {
  nameSector: any;
  constructor(
    public db: AngularFirestore,
    private storage: AngularFireStorage
  ) {}

  setNewUser(user: User) {
    return this.db.collection("users").add({
      email: user.email,
      name: user.name,
      organy: user.organization,
      type: user.type,
      role: user.roles,
      statusActive: user.statusActive,
      temporal: user.password,
      orgName: user.orgName,
      typeOrg: user.typeOrg,
      dateCreation: Date.now(),
      dateModification: Date.now()
    });
  }

  setConfig(key) {
    return this.db.collection("config").add({
      googleKey: key
    });
  }

  setConfigOrg(org) {
    return this.db.collection("config-org").add({
      organi: org
    });
  }

  setConfigCatg(org) {
    return this.db.collection("config-catg").add({
      organi: org
    });
  }

  async getKey() {
    return await new Promise<any[]>(resolve => {
      this.db
        .collection("config")
        .valueChanges()
        .subscribe(post => resolve(post));
    });
  }

  async getOrgs() {
    return await new Promise<any[]>(resolve => {
      this.db
        .collection("config-org")
        .snapshotChanges()
        .subscribe(post => resolve(post));
    });
  }

  async getCatgs() {
    return await new Promise<any[]>(resolve => {
      this.db
        .collection("config-catg")
        .snapshotChanges()
        .subscribe(post => resolve(post));
    });
  }

  async getOrganysActives() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("users", ref =>
          ref.where("role", "==", "ORGANY").where("statusActive", "==", true)
        )
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getUsersOrgany() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("users", ref => ref.where("role", "==", "ORGANY"))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getUsersOrganyOfOrgany(organi) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("users")
        .doc(organi)
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getUsersUser() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("users", ref => ref.where("role", "==", "USER"))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getUsersRoleUser(id) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("users", ref => ref.where("organy", "==", id))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getUserRoleUser(id) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("users")
        .doc(id)
        .snapshotChanges()
        .subscribe(user => resolve(user));
    });
  }

  async getUserMail(mail) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("users", ref => ref.where("email", "==", mail))
        .snapshotChanges()
        .subscribe(varl => resolve(varl));
    });
  }

  setAlert(poin: Points) {
    this.db.collection("alerts").add({
      departamento: poin.ubication.departamento,
      sectores: this.nameSector,
      propietario: poin.propietario,
      capacidad: poin.capacidad,
      isActive: poin.active,
      titlepoint: poin.title,
      descpPoint: poin.desc,
      action: poin.action,
      organy: poin.orgName,
      organyId: poin.organizacion,
      dateCreation: Date.now(),
      dateModification: Date.now()
    });
  }

  getDataPoint(poin: Points) {
    let sectores = this.getSectores(poin.sectores.sector);
    if (sectores.length > 1) {
      poin.multisector = true;
    }

    let grupos = this.getGrupos(poin.grupo.grupos);
    return {
      ubication: {
        departamento: poin.ubication.departamento,
        municipio: poin.ubication.municipio,
        iterante: poin.ubication.iterante,
        fechaInit: poin.ubication.fechaInit.valueOf(),
        fechaFin: poin.ubication.fechaFin.valueOf(),
        address: poin.ubication.address,
        lat: poin.ubication.lat,
        lnt: poin.ubication.lnt
      },
      contact: {
        name: poin.contact.name,
        telefono: poin.contact.telefono,
        email: poin.contact.email
      },
      title: poin.title,
      desc: poin.desc,
      oferta: poin.oferta,
      grupos: grupos,
      borrador: poin.borrador,
      sectores: sectores,
      orgName: poin.orgName,
      status: poin.status,
      listOrgs : poin.listOrg,
      organizacion: poin.organizacion,
      propietario: poin.propietario,
      capacidad: poin.capacidad,
      horarys: this.getHorarys(poin.horarys.dias),
      img: poin.img,
      multisector: poin.multisector,
      active: poin.active,
      dateCreation: Date.now(),
      dateModification: Date.now()
    };
  }

  setNewPoint(poin: Points) {
    console.log(poin);
    let data = this.getDataPoint(poin);
    console.log(data);
    this.setAlert(poin);
    return this.db.collection("points").add(data);
  }

  setNewPost(post: Entradas) {
    return this.db.collection("posts").add({
      title: post.title,
      text: post.text,
      descrip: post.descp,
      category: post.category,
      img: post.img,
      propietario: post.propietrio,
      organizacion: post.organy,
      dateCreation: new Date(),
      dateModification: new Date()
    });
  }

  setImg(file) {
    // Generate a random ID
    const randomId = Math.random()
      .toString(36)
      .substring(2);
    const filepath = `images/${randomId}`;

    const fileRef = this.storage.ref(filepath);

    // Upload image
    return new Promise(resolve => {
      this.storage
        .upload(filepath, file)
        .snapshotChanges()
        .pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe(url => resolve(url));
          })
        )
        .subscribe();
    });
  }

  private getGrupos(grupos) {
    let data = [];
    for (let index = 0; index < grupos.length; index++) {
      if (grupos[index].grup) {
        let dato = {
          sector: grupos[index].name
        };
        data.push(dato);
      }
    }

    return data;
  }

  private getSectores(sectores) {
    let data = [];
    for (let index = 0; index < sectores.length; index++) {
      if (sectores[index].sector) {
        if (!this.nameSector) {
          this.nameSector = sectores[index].name;
        }
        let dato = {
          sector: sectores[index].name
        };
        data.push(dato);
      }
    }

    return data;
  }

  private getHorarys(horarys) {
    let data = [];
    for (let index = 0; index < horarys.length; index++) {
      let dato = {
        dia: horarys[index].dia,
        horaInic: horarys[index].horaInic,
        horaFin: horarys[index].horaFin
      };
      data.push(dato);
    }

    return data;
  }

  async check(user: string) {
    return await this.db
      .collection("users", ref => ref.where("email", "==", user))
      .valueChanges();
  }
  async getPoinsUpdate() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points-updates")
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getPoinsUpdateOfOrgany(organy) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points-updates", ref =>
          ref.where("organizacion", "==", organy)
        )
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getPoinsOfOrgany(organy) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points", ref => ref.where("organizacion", "==", organy))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getPoinsSectorOf(sector) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points", ref => ref.where("sectores.name", "==", sector))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getPoinsOfPropietario(propietario) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points", ref =>
          ref.where("propietario", "==", propietario)
        )
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getAlerts() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("alerts")
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getAlertsOfOrgany(organy) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("alerts", ref => ref.where("organyId", "==", organy))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getAlertsOfUser(user) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("alerts", ref => ref.where("propietario", "==", user))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getAllPostValues() {
    return await new Promise<any[]>(resolve => {
      this.db
        .collection("posts")
        .snapshotChanges()
        .pipe(
          map(actions =>
            actions.map(a => {
              const data = a.payload.doc.data() as Post[];
              const id = a.payload.doc.id;
              return { id, ...data };
            })
          )
        )
        .subscribe(post => resolve(post));
    });
  }

  async getPostById(id: string) {
    // tslint:disable-next-line: no-shadowed-variable
    return await new Promise<Post>(resolve => {
      this.db
        .collection("posts")
        .doc(id)
        .valueChanges()
        .subscribe(resolve);
    });
  }

  async getAllCategoriesConfigValues() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("config-catg")
        .valueChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getAllPoinsValues() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points", ref => ref.where("active", "==", true))
        .valueChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getAllPoins() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points", ref => ref.where("active", "==", true))
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  async getAllPoinsAdmin() {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points")
        .snapshotChanges()
        .subscribe(points => resolve(points));
    });
  }

  // async getAllPoinsValuesCitysAndTypes(city, type) {
  //   return await new Promise<any>(resolve => {
  //     this.db
  //     .collection("points", ref => ref.where("ubication.", "==", propietario))
  //     .snapshotChanges()
  //     .subscribe(points => resolve(points));
  //   });
  // }

  async updateUser(user, data) {
    this.db
      .collection("users")
      .doc(user)
      .set(data);
  }

  async updatePoint(point, dataPoint: Points) {
    let data = this.getDataPoint(dataPoint);
    this.setAlert(dataPoint);
    this.db
      .collection("points")
      .doc(point)
      .set(data);
  }

  async updatePointUser(point, dataPoint: Points) {
    let data = this.getDataPoint(dataPoint);
    data["idUpdate"] = point;
    this.setAlert(dataPoint);
    this.db
      .collection("points-updates")
      .add(data)
      .catch(() => {
        return data;
      });
  }

  async activedPoint(point, data) {
    this.db
      .collection("points")
      .doc(point)
      .set(data);
  }

  async getPoint(point) {
    return await new Promise<any>(resolve => {
      this.db
        .collection("points")
        .doc(point)
        .valueChanges()
        .subscribe(post => resolve(post));
    });

    // return await this.db
    //     .collection("points")
    //     .doc(point).get();
  }

  async deleteUser(user) {
    this.db
      .collection("users")
      .doc(user)
      .delete();
  }

  async deletePost(post) {
    this.db
      .collection("posts")
      .doc(post)
      .delete();
  }

  async deleteOrg(org) {
    this.db
      .collection("config-org")
      .doc(org)
      .delete();
  }

  async deleteCat(org) {
    this.db
      .collection("config-catg")
      .doc(org)
      .delete();
  }

  async deletePoint(point) {
    this.db
      .collection("points")
      .doc(point)
      .delete();
  }

  async deletePointUpdate(point) {
    this.db
      .collection("points-updates")
      .doc(point)
      .delete();
  }
}
