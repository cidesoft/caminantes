interface Post {
  id: string;
  title: string;
  descrip: string;
  img: string;
  category: string;
  text: string;
  dateCreation: DateCreation;
  dateModification: DateCreation;
  organizacion: string;
  propietario: string;
}

interface DateCreation {
  seconds: number;
  nanoseconds: number;
}