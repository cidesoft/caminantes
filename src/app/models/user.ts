export class User {
  orgName : string;
  name: string;
  email: string;
  organization: string;
  type: Number;
  types: Number;
  password: string;
  roles : string;
  statusActive : boolean;
  typeOrg: string; 
}
