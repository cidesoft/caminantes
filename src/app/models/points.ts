import { Ubicacion } from './ubicacion';
import { Sectores } from './sectores';
import { Horarys } from './horarys';
import { Grupos } from './grupos';
import { Contact } from './contact';

export class Points {
   title : string;
   desc : string;
   ubication : Ubicacion = new Ubicacion();
   sectores : Sectores = new Sectores();
   contact : Contact = new Contact();
   grupo : Grupos = new Grupos();
   horarys : Horarys = new Horarys;
   listOrg : any;
   capacidad : any;
   oferta : any;
   img : any;
   propietario : any;
   organizacion : any;
   orgName : any;
   status : any;
   multisector : boolean = false;
   active : boolean;
   borrador : boolean;
   action : any;
 
   
}
