import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegistryComponent } from './components/registry/registry.component';
import { ConfirmedComponent } from './components/confirmed/confirmed.component';
import { AdminComponent } from './components/admin/admin.component';
import { BlogComponent } from './components/blog/blog.component';
import { AdminRoutes } from './components/main-admin-nav/main-admin-routes';
import { HomeRoutes } from './components/home/home-router';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent, children: HomeRoutes },
  { path: 'registro', component: RegistryComponent },
  { path: 'confirm', component: ConfirmedComponent },
  { path: 'admin', component: AdminComponent, children: AdminRoutes },
  { path: 'blog', component: BlogComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
